import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: [
  ]
})
export class PerfilComponent implements OnInit {

  usuario: string;
  rol: string

  constructor() { }

  ngOnInit(): void {

    const helper = new JwtHelperService();
    let token = sessionStorage.getItem(environment.TOKEN_NAME);

    const decodedToken = helper.decodeToken(token);

    this.usuario = decodedToken.user_name;  
    this.rol = "";
    for(var i = 0; i<decodedToken.authorities.length - 1; i++) {
      this.rol += decodedToken.authorities[i]+",";
    }

    this.rol += decodedToken.authorities[decodedToken.authorities.length-1];


  }

}
