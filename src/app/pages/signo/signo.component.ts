import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Signo } from 'src/app/_model/signo';
import { SignoService } from 'src/app/_service/signo.service';

@Component({
  selector: 'app-signo',
  templateUrl: './signo.component.html',
  styleUrls: ['./signo.component.css']
})
export class SignoComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<Signo>;
  displayedColumns: string[] = ['idSigno', 'idPaciente', 'fecha', 'temperatura', 'pulso', 'ritmo', 'acciones'];
  cantidad: number = 0;

  constructor(
    private signoService: SignoService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {

    this.signoService.listarPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });

    this.signoService.getSignoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.signoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "right"
      });
    });
  }

  eliminar(id: number) {
    this.signoService.eliminar(id).subscribe(() => {
      this.signoService.listar().subscribe(data => {
        this.signoService.setSignocambio(data);
        this.signoService.setMensajeCambio('SE ELIMINO');
      });
    });
  }


  crearTabla(data: Signo[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  mostrarMas(e: any){
    this.signoService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }
}
